import random


class Loup:

    def __init__(self, gain_nourriture, x, y, t_reproduction, monde, moutons):
        self.gain_nourriture = gain_nourriture
        self.position_x = x
        self.position_y = y
        self.energie = round(random.uniform(1, 2), 1) * gain_nourriture
        self.t_reproduction = t_reproduction
        self.univ = monde
        self.moutons = moutons

    def variationEnergie(self):
        ind = 0
        tuer = 1
        for a in self.moutons:
            if tuer != 0:
                if self.position_x == a.position_x and self.position_y == a.position_y:
                    self.energie += self.gain_nourriture
                    self.moutons.pop(ind)
                    tuer -= 1
            ind += 1
        if tuer == 1:
            self.energie -= 1

    def deplacement(self):
        self.position_x += random.randint(-1, 1)
        self.position_y += random.randint(-1, 1)
        self.position_x, self.position_y = self.position_x % self.univ.dimension, self.position_y % self.univ.dimension

    def place_mouton(self, i, j):
        return Loup(self.gain_nourriture, i%self.dimension, j%self.dimension, self.t_reproduction, self.univ)
