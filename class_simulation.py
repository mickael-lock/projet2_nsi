import class_mouton
import class_loup
import random


class Simulation:

    def __init__(self, nb_mouton, nb_loup, fin_du_monde, moutons, loups, chasseurs, mouton_max, monde):
        self.nb_mouton = nb_mouton
        self.moutons = moutons  # moutons est une liste d'objet Mouton
        self.loups = loups  # loups  est une liste d'objet Loup
        self.chasseurs = chasseurs  # chasseurs  est une liste d'objet Chasseur
        self.horloge = 0
        self.__fin_du_monde = fin_du_monde
        self.univ1 = monde
        self.resultat_herbe = [self.univ1.nbHerbe()]
        self.resultat_mouton = [nb_mouton]
        self.resultat_loup = [nb_loup]
        self.mouton_max = mouton_max

    def simMouton(self):
        while self.horloge < self.__fin_du_monde:
            # aumgmente l'horloge
            self.horloge += 1

            # fait pousser l'herbe
            self.univ1.herbePousse()

            # variation d'énergie des moutons
            ind = 0
            for i in self.moutons:
                i.variationEnergie(i.position_x, i.position_y)
                if i.energie == 0:
                    self.moutons.pop(ind)
                ind += 1

            # variation d'énergie des loups
            if len(self.loups) != 0:
                ind = 0
                for i in self.loups:
                    i.variationEnergie()
                    if i.energie == 0:
                        self.loups.pop(ind)
                    ind += 1

            # chasseur tue un loup
            if len(self.chasseurs) != 0:
                for i in self.chasseurs:
                    i.tueLoup()

            # reproduction des moutons
            for i in self.moutons:
                for j in self.moutons:
                    if i.position_x == j.position_x and i.position_y == j.position_y:
                        if random.randint(0, 100) < i.t_reproduction:
                            self.moutons.append(class_mouton.Mouton(i.gain_nourriture, i.position_x, i.position_y, i.t_reproduction,self.univ1))

            # reproduction des loups
            if len(self.loups) != 0:
                for i in self.loups:
                    for j in self.loups:
                        if i.position_x == j.position_x and i.position_y == j.position_y:
                            if random.randint(0, 100) < i.t_reproduction:
                                self.loups.append(class_loup.Loup(i.gain_nourriture, i.position_x, i.position_y,i.t_reproduction, self.univ1, self.moutons))

            # déplacement des moutons
            for i in self.moutons:
                i.deplacement()

            # déplacement des loups
            for i in self.loups:
                i.deplacement()

            # déplacement des chasseurs
            for i in self.chasseurs:
                i.deplacement()

            # sauvegarder dans resultat_herbe, resultat_mouton et resultat_loup
            self.resultat_herbe.append(self.univ1.nbHerbe())
            self.resultat_mouton.append(len(self.moutons))
            self.resultat_loup.append((len(self.loups)))

            # arrêt de la simulation s'il n'y a plus de moutons, ou s'il y a plus que mouton_max
            if len(self.moutons) == 0 or len(self.moutons) > self.mouton_max:
                self.horloge = self.__fin_du_monde
        return self.resultat_mouton, self.resultat_herbe, self.resultat_loup
