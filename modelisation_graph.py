import class_monde
import class_mouton
import class_loup
import class_chasseur
import class_simulation
import random
import matplotlib.pyplot as plt


def modelisation_graph(dimension, duree_repousse, gain_nourritureM, gain_nourritureL, t_reproductionM, t_reproductionL, nb_mouton, nb_loup, nb_chasseur, fin_du_monde, mouton_max):
    monde = class_monde.Monde(dimension, duree_repousse)
    moutons = []
    loups = []
    chasseurs = []
    for i in range(nb_mouton):
        moutons.append(class_mouton.Mouton(gain_nourritureM, random.randint(0, dimension-1), random.randint(0, dimension-1), t_reproductionM, monde))
    for i in range(nb_loup):
        loups.append(class_loup.Loup(gain_nourritureL, random.randint(0, dimension-1), random.randint(0, dimension-1), t_reproductionL, monde, moutons))
    for i in range(nb_chasseur):
        chasseurs.append(class_chasseur.Chasseur(random.randint(0, dimension-1), random.randint(0, dimension-1), monde, loups))
    simulation1 = class_simulation.Simulation(nb_mouton, nb_loup, fin_du_monde, moutons, loups, chasseurs, mouton_max, monde)
    resultat_mouton, resultat_herbe, resultat_loup = simulation1.simMouton()
    x = [i for i in range(len(resultat_herbe))]
    plt.plot(x, resultat_mouton, color="red", label="Moutons")
    plt.plot(x, resultat_herbe, color="green", label="Herbe")
    plt.plot(x, resultat_loup, color="grey", label="Loups")
    plt.show()


modelisation_graph(30, 30, 4, 18, 4, 5, 40, 20, 25, 75, 600)



