import random


class Monde:
    """
    dimension = entier, min 50, taille du côté de la matrice
    duree_pousse = entier entre 1 et 100, vitesse de repousse herbe
    carte = liste de liste, matrice de dimension dimension(variable en haut) + c'est une matrice carré
    """

    def __init__(self, dimension, duree_repousse):
        self.duree_repousse = duree_repousse
        self.dimension = dimension
        self.carte = []
        # initialisation de la carte avec 50% de carrés herbus
        pourcentage = 0
        for i in range(self.dimension):
            carte2 = []
            for j in range(self.dimension):
                if pourcentage < (self.dimension**2) // 2:
                    carte2.append(self.duree_repousse)
                    pourcentage += 1
                else:
                    carte2.append(random.randint(0, self.duree_repousse-1))
            self.carte.append(carte2)

    def nbHerbe(self):
        cpt = 0
        for i in range(len(self.carte)):
            for j in range(len(self.carte)):
                if self.carte[i][j] == self.duree_repousse:
                    cpt += 1
        return cpt

    def herbePousse(self):
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.carte[i][j] < self.duree_repousse:
                    self.carte[i][j] += 1

    def herbeMangee(self, i, j):
        if self.carte[i%self.dimension][j%self.dimension] >= self.duree_repousse:
            self.carte[i%self.dimension][j%self.dimension] = 0

    def getCoefCarte(self, i, j):
        return self.carte[i%self.dimension][j%self.dimension]
