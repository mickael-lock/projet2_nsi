import random


class Mouton:

    def __init__(self, gain_nourriture, x, y, t_reproduction, monde):
        self.gain_nourriture = gain_nourriture
        self.position_x = x
        self.position_y = y
        self.energie = round(random.uniform(1, 2), 1) * gain_nourriture
        self.t_reproduction = t_reproduction
        self.univ = monde

    def variationEnergie(self, i, j):
        if self.univ.getCoefCarte(i, j) == self.univ.duree_repousse:
            self.energie += self.gain_nourriture
            self.univ.herbeMangee(i, j)
        else:
            self.energie -= 1
        return self.energie

    def deplacement(self):
        self.position_x += random.randint(-1, 1)
        self.position_y += random.randint(-1, 1)
        self.position_x, self.position_y = self.position_x % self.univ.dimension, self.position_y % self.univ.dimension

    def place_mouton(self, i, j):
        return  Mouton(self.gain_nourriture, i%self.dimension, j%self.dimension, self.t_reproduction, self.univ)
