# Projet2_NSI

Bonjour dans ce projet, les programmes que nous avons codé permettent de simuler un monde composé de moutons, de loups et de chasseurs.

# Classe Monde
    # Attributs de la classe
    - dimension : 
    - duree_repousse : 
    
    # Méthodes de la classe
    - nbHerbe : 
    - herbePousse : 
    - herbeMangee :
    - getCoefCarte :
    
# Classe Mouton
    # Attributs de la classe
    - gain_nourriture : 
    - x :
    - y :
    - t_reproduction :
    - monde :

    # Méthodes de la classe
    - variationEnergie : 
    - deplacement :
    - place_mouton :

# Classe Loup
    # Attributs de la classe
    - gain_nourriture :
    - x :
    - y :
    - t_reproduction : 
    - monde :
    - moutons :

    # Méthodes de la classe
    - variationEnergie :
    - deplacement :
    - place_loup :

# Classe Chasseur
    # Attributs de la classe
    - x :
    - y :
    - monde :
    - loups :

    # Méthodes de la classe
    - tueLoup :
    - deplacement :
    
# Classe Simulation
    # Attributs de la classe
    - nb_mouton :
    - nb_loup :
    - fin_du_monde :
    - moutons :
    - loups :
    - chasseurs :
    - mouton_max :
    - monde :
    
    # Méthodes de la classe
    - simMouton :


