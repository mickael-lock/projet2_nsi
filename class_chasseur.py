import random


class Chasseur:

    def __init__(self, x, y, monde, loups):
        self.position_x = x
        self.position_y = y
        self.univ = monde
        self.loups = loups

    def tueLoup(self):
        ind = 0
        tuer = 1
        for i in self.loups:
            if tuer != 0:
                if self.position_x == i.position_x and self.position_y == i.position_y:
                    self.loups.pop(ind)
                    tuer -= 1
            ind += 1

    def deplacement(self):
        self.position_x += random.randint(-1, 1)
        self.position_y += random.randint(-1, 1)
        self.position_x, self.position_y = self.position_x % self.univ.dimension, self.position_y % self.univ.dimension
        self.position_x, self.position_y = self.position_x % self.univ.dimension, self.position_y % self.univ.dimension
